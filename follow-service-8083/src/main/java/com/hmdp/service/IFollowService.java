package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.Follow;
import com.entity.Result;

public interface IFollowService extends IService<Follow> {

    Result follow(Long follUserId, Boolean isFollow);

    Result isFollow(Long followUserId);


    Result followCommons(Long id);

//    Result sendUser(String phone, HttpSession session);
}
