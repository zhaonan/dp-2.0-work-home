package com.hmdp.controller;


import com.client.UserClient;
import com.entity.Follow;
import com.entity.Result;
import com.hmdp.service.IFollowService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 */
@RestController
@RequestMapping("/follow")
public class FollowController {

    @Resource
    private IFollowService followService;

    @Resource
    private UserClient client;

//    @HystrixCommand(fallbackMethod = "sendUSerFallbackMethod")
    @PostMapping("code")
    public Result sendUSer(@RequestParam String phone, HttpSession session){
        System.out.println("11111");
//        Result r1 = followService.sendUser(phone, session);
        Result r1 = client.sendCode(phone);
//        System.out.println(r1);
//        Result r1 = client.me();
        return r1;
    }
    @GetMapping("/{id}")
    public Result getUserById(@PathVariable("id") Long id){
        return client.queryUserById(id);
    }

    @PutMapping("/{id}/{isFollow}")
    public Result follow(@PathVariable("id") Long follUserId, @PathVariable("isFollow") Boolean isFollow){
        return followService.follow(follUserId, isFollow);
    }

    @GetMapping("/or/not/{id}")
    public Result follow(@PathVariable("id") Long followUserId){
        return followService.isFollow(followUserId);
    }

    @GetMapping("common/{id}")
    public Result followCommons(@PathVariable("id") Long id){
        return followService.followCommons(id);
    }

    @GetMapping("/api/{id}")
    public List<Follow> getFollowsById(@PathVariable("id") Long id){
        return followService.query().eq("follow_user_id", id).list();
    }

}
