package com.hmdp.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.Result;
import com.entity.VoucherOrder;
import com.hmdp.mapper.VoucherOrderMapper;
import com.hmdp.service.ISeckillVoucherService;
import com.hmdp.service.IVoucherOrderService;
import com.hmdp.utils.RedisIdWorker;
import com.hmdp.utils.UserHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * 
 * 
 */
@Service
@Slf4j
public class VoucherOrderServiceImpl extends ServiceImpl<VoucherOrderMapper, VoucherOrder> implements IVoucherOrderService {

    @Resource
    private ISeckillVoucherService seckillVoucherService;
    @Resource
    private RedisIdWorker redisIdWorker;  // 生成订单id
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Resource
    private RedissonClient redissonClient;
    @Resource
    private RabbitTemplate rabbitTemplate;

    private static final DefaultRedisScript<Long> SECKILL_SCRIP;
    static {  //由于UNLOCK_SCRIPT使用静态修饰，所以用静态代码块初始化
        SECKILL_SCRIP = new DefaultRedisScript<>();
        SECKILL_SCRIP.setLocation(new ClassPathResource("seckill.lua"));
        SECKILL_SCRIP.setResultType(Long.class);
    }


    @Override
    public Result seckillVoucher(Long voucherId){  // 基于阻塞队列的异步秒杀
        // 获取用户
        Long userId = UserHolder.getUser().getId();
        // 1.执行Lua脚本
        Long result = stringRedisTemplate.execute(
                SECKILL_SCRIP,
                Collections.emptyList(),
                voucherId.toString(),
                userId.toString()
        );
        // 2.判断是否为0
        assert result != null;
        int r = result.intValue();
        if(r != 0) {
            // 2.1 不为0，没有购买资格
            return Result.fail(r == 1 ? "库存不足" : "不能重复下单");
        }
        // 2.2 为0，有购买资格，把下单信息保存到阻塞队列
        VoucherOrder voucherOrder = new VoucherOrder();
        // 2.3订单id
        long orderId = redisIdWorker.nextId("order");
        voucherOrder.setId(orderId);
        // 2.4用户id
        voucherOrder.setUserId(userId);
        // 2.5代金券id
        voucherOrder.setVoucherId(voucherId);
        // 2.6 获取当前headers，并组合如消息中
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        String header = "";
        if(attributes != null){
            HttpServletRequest request = attributes.getRequest();
            header = "%" + request.getHeader("Authorization");
        }
        // 2.7 加入消息队列
        String queueName = "orderQueue";
        String orderStr = JSONObject.toJSONString(voucherOrder) + header;
        rabbitTemplate.convertAndSend(queueName, orderStr);

        // 3.返回订单id
        return Result.ok(orderId);
    }

}
