package com.hmdp.controller;


import com.entity.Result;
import com.entity.VoucherOrder;
import com.hmdp.service.IVoucherOrderService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 */
@RestController
@RequestMapping("/voucher-order")
public class VoucherOrderController {

    @Resource
    private IVoucherOrderService voucherOrderService;

    @PostMapping("seckill/{id}")
    public Result seckillVoucher(@PathVariable("id") Long voucherId) {
        return voucherOrderService.seckillVoucher(voucherId);
    }
    @PostMapping("/api/save")
    public void Save(@RequestBody VoucherOrder order){
        voucherOrderService.save(order);
    }

    @GetMapping("/api/count")
    public int Query(@RequestParam("user_id") Long user_id, @RequestParam("voucher_id") Long voucher_id){
        return voucherOrderService.query().eq("user_id", user_id).eq("voucher_id", voucher_id).count();
    }
}
