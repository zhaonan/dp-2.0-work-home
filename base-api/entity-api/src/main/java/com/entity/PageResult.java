package com.entity;

import lombok.Data;

import java.util.List;

@Data
public class PageResult {
    private  Long total;
    private List<Shop> shops;

    public PageResult(Long total, List<Shop> shops) {
        this.total = total;
        this.shops = shops;
    }
}
