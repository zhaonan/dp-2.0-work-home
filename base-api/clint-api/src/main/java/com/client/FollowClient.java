package com.client;

import com.entity.Follow;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "followservice", fallback = FollowServerClientFallback.class)
public interface FollowClient {
    @GetMapping(value = "/follow/api/{id}", headers = {"Authorization=fe118cc03e2c454bb12b2bf7da6a4a6e"})
    List<Follow> getFollowsById(@PathVariable("id") Long id);
}




