package com.client;

import com.entity.VoucherOrder;
import org.springframework.stereotype.Component;

@Component
public class VoucherOrderServerClientFallback implements VoucherOrderClient {
    @Override
    public void Save(VoucherOrder order) {
        System.out.println("save fallback");
    }

    @Override
    public int Query(Long user_id, Long voucher_id) {
        System.out.println("fallback");
        return -1;
    }
}
