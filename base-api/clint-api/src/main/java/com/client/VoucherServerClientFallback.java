package com.client;

import org.springframework.stereotype.Component;

@Component
public class VoucherServerClientFallback implements VoucherClient {

    @Override
    public boolean Update(Long voucherId) {
        System.out.println("fallback");
        return true;
    }
}
