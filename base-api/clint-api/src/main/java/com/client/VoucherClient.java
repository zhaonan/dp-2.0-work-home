package com.client;

import com.entity.Follow;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "voucherservice", fallback = VoucherServerClientFallback.class)
public interface VoucherClient {
    @PostMapping("/voucher/api/update/{id}")
    boolean Update(@PathVariable("id") Long voucherId);
}
