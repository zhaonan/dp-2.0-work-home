package com.client;

import com.entity.Result;
import com.entity.User;
import com.entity.UserDTO;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserServerClientFallback implements UserClient {

    @Override
    public List<User> listByIds(List<Long> ids) {
        System.out.println("fall back");
        return null;
    }

    @Override
    public Result sendCode(String phone) {
        System.out.println("fall back");
        return Result.fail("Hystrix fallback, target service nor available");
    }

    @Override
    public Result me() {
        System.out.println("fall back");
        return Result.fail("Hystrix fallback, target service nor available");
    }

    @Override
    public Result queryUserById(Long userId) {
        System.out.println("fall back");
        return Result.fail("Hystrix fallback, target service nor available");
    }

    @Override
    public User queryById(Long userId) {
        System.out.println("fall back");
        return null;
    }

    @Override
    public List<UserDTO> queryUserForBlogLikes(List<Long> ids) {
        System.out.println("fall back");
        return null;
    }
}
