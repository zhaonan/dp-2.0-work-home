package com.client;

import com.entity.Follow;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class FollowServerClientFallback implements FollowClient {
    @Override
    public List<Follow> getFollowsById(Long id) {
        System.out.println("fallback");
        return null;
    }
}
