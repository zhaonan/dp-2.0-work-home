package com.client;

import com.entity.LoginFormDTO;
import com.entity.Voucher;
import com.entity.VoucherOrder;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "voucherorderservice", fallback = VoucherOrderServerClientFallback.class)
public interface VoucherOrderClient {
    @PostMapping("/voucher-order/api/save")
    void Save(@RequestBody VoucherOrder order);
    @GetMapping("/voucher-order/api/count")
    int Query(@RequestParam("user_id") Long user_id, @RequestParam("voucher_id") Long voucher_id);
}

