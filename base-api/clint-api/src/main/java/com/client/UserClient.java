package com.client;

import com.entity.Result;
import com.entity.User;
import com.entity.UserDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpSession;
import java.util.List;

@FeignClient(name = "userservice",  fallback = UserServerClientFallback.class)
public interface UserClient {
    @GetMapping("/user/api/list")
    List<User> listByIds(@RequestParam("ids") List<Long> ids);

    @PostMapping(value = "/user/code")
    Result sendCode(@RequestParam("phone") String phone);

    @GetMapping(value = "/user/me")
    Result me();

    @GetMapping(value = "/user/{id}")
    Result queryUserById(@PathVariable("id") Long userId);

    @GetMapping(value = "/user/api/{id}")
    User queryById(@PathVariable("id") Long userId);

    @GetMapping(value = "/user/api/blogLikes")
    List<UserDTO> queryUserForBlogLikes(@RequestParam("ids") List<Long> ids);
}



