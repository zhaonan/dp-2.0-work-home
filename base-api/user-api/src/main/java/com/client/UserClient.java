package com.client;

import com.entity.Result;
import com.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


import javax.servlet.http.HttpSession;
import java.util.List;

@FeignClient(name = "userservice")
public interface UserClient {
    @GetMapping("/user/list")
    List<User> listByIds(@RequestParam("ids") List<Long> ids);

    @PostMapping(value="/user/code")
    Result sendCode(@RequestParam("phone") String phone);

    @GetMapping(value = "/user/me", headers = {"Authorization=fe118cc03e2c454bb12b2bf7da6a4a6e"})
    Result me();

    @GetMapping(value = "/user/{id}", headers = {"Authorization=fe118cc03e2c454bb12b2bf7da6a4a6e"})
    Result queryUserById(@PathVariable("id") Long userId);
}
