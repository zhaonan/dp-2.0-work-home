package com.example.gateway10010;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Gateway10010Application {

    public static void main(String[] args) {
        SpringApplication.run(Gateway10010Application.class, args);
    }

}
