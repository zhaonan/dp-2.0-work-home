# 概述

本项目为类美团项目dp1.0改进的微服务版本，包括了博客服务、异步下单服务、倒排索引搜索服务、粉丝服务、订单处理服务、店铺服务、用户服务、优惠券服务和优惠券秒杀服务。
注册中心：eureka，网关：gateway，负载均衡：openfeign集成的ribbon，异步消息队列：rabbitMQ，分词搜索引擎：ElasticSearch。

# 配置

- redis:7.0.8.1
- nginx:1.18.0
- mysql:5.6
- rabbitmq:3-management
- es:7.12.1
- kibana:7.12.1
- ik分词器:7.12.1

