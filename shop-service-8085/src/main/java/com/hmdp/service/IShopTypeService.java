package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.Result;
import com.entity.ShopType;

/**
 * <p>
 *  服务类
 * </p>
 *
 * 
 * 
 */
public interface IShopTypeService extends IService<ShopType> {
    Result queryAll();
}
