package com.hmdp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.Shop;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * 
 * 
 */
public interface ShopMapper extends BaseMapper<Shop> {

}
