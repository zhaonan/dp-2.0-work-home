package com.hmdp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.ShopType;

public interface ShopTypeMapper extends BaseMapper<ShopType> {

}
