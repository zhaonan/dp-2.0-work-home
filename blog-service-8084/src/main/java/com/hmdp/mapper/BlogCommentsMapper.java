package com.hmdp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.BlogComments;

/**
 * <p>
 *  Mapper 接口
 * </p>
 */
public interface BlogCommentsMapper extends BaseMapper<BlogComments> {

}
