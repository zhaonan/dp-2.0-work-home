package com.hmdp.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.Blog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 */
public interface BlogMapper extends BaseMapper<Blog> {

}
