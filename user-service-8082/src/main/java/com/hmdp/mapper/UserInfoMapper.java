package com.hmdp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.entity.UserInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * 
 * 
 */
@Mapper
public interface UserInfoMapper extends BaseMapper<UserInfo> {

}
