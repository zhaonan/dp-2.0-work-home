package com.hmdp.utils;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 判断是否需要拦截（ThreanLocal中是否有用户）
        if(com.hmdp.utils.UserHolder.getUser() == null){  // 此处必须用UserHolder的全限名，否则会找不到类

            response.setStatus(401);

            return false;
        }
        // 有用户，放行
        return true;
    }
}
