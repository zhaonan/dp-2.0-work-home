package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.LoginFormDTO;
import com.entity.Result;
import com.entity.User;
import com.entity.UserDTO;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * 
 * 
 */
public interface IUserService extends IService<User> {

    Result sendCode(String phone, HttpSession session);

    Result login(LoginFormDTO loginForm, HttpSession session);

    Result sign();

    Result signCount();

    List<UserDTO> userBlogLikes(List<Long> ids);
}
