package com.hmdp.controller;


import com.entity.Result;
import com.entity.Shop;
import com.hmdp.service.IEsService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * 
 * 
 */
@RestController
@RequestMapping("/es")
public class EsController {

    @Resource
    public IEsService esService;


    @GetMapping("/list")
    public Result search(@RequestParam(value = "esContent", required = false) String esContent){
        System.out.println(esContent);
        return esService.search(esContent);
    }

    @PostMapping("/filters")
    public Map<String, List<String>> getFilters(@RequestBody Shop params){
        return null;
    }

    @GetMapping("/suggestion")
    public Result suggestion(@RequestParam("suggestion") String suggestion){
        return Result.ok();
    }
}
