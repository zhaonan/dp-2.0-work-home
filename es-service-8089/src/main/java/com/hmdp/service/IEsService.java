package com.hmdp.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.entity.Result;
import com.entity.Shop;

/**
 * <p>
 *  服务类
 * </p>
 *
 * 
 * 
 */
public interface IEsService extends IService<Shop> {

    Result update(Shop shop);

    Result search(String esContent);
}
