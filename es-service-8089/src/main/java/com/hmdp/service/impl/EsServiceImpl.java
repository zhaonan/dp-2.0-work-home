package com.hmdp.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.entity.Result;
import com.entity.Shop;
import com.hmdp.mapper.ShopMapper;
import com.hmdp.service.IEsService;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static com.hmdp.utils.RedisConstants.CACHE_SHOP_KEY;

@Service
public class EsServiceImpl extends ServiceImpl<ShopMapper, Shop> implements IEsService {

    @Resource
    StringRedisTemplate stringRedisTemplate;

    @Autowired
    private RestHighLevelClient client;

    @Override
    @Transactional
    public Result update(Shop shop) {
        Long id = shop.getId();
        if(id == null)
            return Result.fail("id 不能为空");
        // 1.更新数据库
        updateById(shop);
        // 2.删除缓存
        stringRedisTemplate.delete(CACHE_SHOP_KEY + id);
        return Result.ok();
    }

    @Override
    public Result search(String esContent){
        try {
            // 准备request
            SearchRequest request = new SearchRequest("shop");
            // 准备DSL
            // query
            request.source().query(QueryBuilders.matchQuery("all", esContent));
            // 发送请求
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            // 解析请求
            return handleResponse(response);

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Result handleResponse(SearchResponse response) throws IOException {
        // 解析相应
        SearchHits searchHits = response.getHits();
        // 获取总条数
        long total = searchHits.getTotalHits().value;
        // 文档数组
        SearchHit[] hits = searchHits.getHits();
        List<Shop> shops = new ArrayList<>();
        for (SearchHit hit : hits) {
            // 获取文档source
            String json = hit.getSourceAsString();
            // 反序列化
            Shop shop = JSON.parseObject(json, Shop.class);
            // 获取排序值
            Object[] sortValues = hit.getSortValues();
            if(sortValues.length > 0){
                Object sortValue = sortValues[0];
                shop.setDistance((Double) sortValue);
            }
            System.out.println("searched shop:" + shop.toString());
            shops.add(shop);
        }
        return Result.ok(shops);
    }

}
