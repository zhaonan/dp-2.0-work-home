package hmdp.orderhandleservice8088;

import org.junit.jupiter.api.Test;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;

@SpringBootTest
@ContextConfiguration
class OrderHandleServiceApplicationTests {

    @Resource
    RabbitTemplate rabbitTemplate;
//    @Test
//    void contextLoads() {
//    }
    @Test
    public void receive() {
        Object o = rabbitTemplate.receiveAndConvert("orderQueue");
//        System.out.println(o.hashCode());
        System.out.println(o);
    }

}
