package hmdp;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.context.request.RequestContextListener;

@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan("com.hmdp.mapper")
@SpringBootApplication
@EnableFeignClients(basePackages = {"com.client"})
@ComponentScan({"com.client", "com.hmdp", "hmdp.controller"})
public class VoucherHandleServiceApplication {

    @Bean
    public RequestContextListener requestContextListener(){
        return new RequestContextListener();
    }
    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(VoucherHandleServiceApplication.class);
        springApplication.setAllowBeanDefinitionOverriding(true);  // 当yml的Spring.main.SetAllowBeanDefinitionOverriding=true失效时用
        springApplication.run(args);
    }

//    @Bean
//    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {  // 查看注入的bean
//        return args -> {
//
//            System.out.println("Let's inspect the beans provided by Spring Boot:");
//
//            String[] beanNames = ctx.getBeanDefinitionNames();
//            Arrays.sort(beanNames);
//            for (String beanName : beanNames) {
//                System.out.println(beanName);
//            }
//
//        };
//    }
}
