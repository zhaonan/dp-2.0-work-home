package hmdp.utils;


public class HeaderHolder {
    private static final ThreadLocal<String > t2 = new ThreadLocal<>();

    public static void saveHeader(String headerMap){
        t2.set(headerMap);
    }

    public static String getHeader(){
        return t2.get();
    }

    public static void removeHeader(){
        t2.remove();
    }
}