package hmdp.controller;

import com.alibaba.fastjson.JSONObject;
import com.client.VoucherClient;
import com.client.VoucherOrderClient;
import com.entity.VoucherOrder;
import hmdp.utils.HeaderHolder;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;

import javax.annotation.Resource;


@Slf4j
@Component
public class SpringRabbitListener {

    @Resource
    private RedissonClient redissonClient;
    @Resource
    private VoucherClient voucherClient;
    @Resource
    private VoucherOrderClient voucherOrderClient;

    @RabbitListener(queues = "orderQueue")
    public void listenSimpleQueueMessage(String orderStr) throws InterruptedException
    {

        if(orderStr.contains("%")){
            System.out.println("orderStr:" + orderStr);
            int index = orderStr.indexOf("%");
            String token = orderStr.substring(index + 1, orderStr.length());
            HeaderHolder.saveHeader(token);
            orderStr = orderStr.substring(0, index);
        }
        VoucherOrder order = JSONObject.parseObject(orderStr, VoucherOrder.class);
        handleVoucherOrder(order);
    }

    private void handleVoucherOrder(VoucherOrder voucherOrder){  // 异步处理，不要返回值
        // 1.获取用户
        Long userId = voucherOrder.getUserId();
        // 2.创建锁对象
        RLock lock = redissonClient.getLock("lock:order:" + userId);
        // 3.获取锁
        boolean isLock = lock.tryLock();
        // 4.判断是否获取锁成功
        if(!isLock){
            // 获取锁失败，返回错误或重试
            log.error("不允许重复下单");
            return;
        }
        try{
            createVoucherOrder(voucherOrder);
        }finally {
            // 释放锁
            lock.unlock();
        }
    }

    @Transactional
    public void createVoucherOrder(VoucherOrder voucherOrder) {  // 悲观锁
        // 5.一人一单
        // 5.1 查询订单
        Long userId =voucherOrder.getUserId();
        int count = voucherOrderClient.Query(userId, voucherOrder.getVoucherId());
        // 5.2 判断是否存在
        if (count > 0) {
            log.error("用户已经购买过一次");
            return;
        }
        RequestContextHolder.setRequestAttributes(RequestContextHolder.getRequestAttributes(), true);
        boolean success = voucherClient.Update(voucherOrder.getVoucherId());
        System.out.println(success);
        if (!success) {
            // 扣减失败
            log.error("库存不足");
            return;
        }
        // 7.创建订单
        voucherOrderClient.Save(voucherOrder);
        System.out.println("save order success");
    }
}
